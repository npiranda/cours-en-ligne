import { Component, OnInit } from '@angular/core';
import {environment} from '../../environments/environment';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  map: mapboxgl.Map;
  marker: mapboxgl.Marker;
  popup: mapboxgl.Popup;
  style = 'mapbox://styles/mapbox/streets-v11';
  lat = 47.49565128732645;
  lng = 6.803409212776291;
  constructor() { }

  ngOnInit(): void {
    // @ts-ignore
    mapboxgl.accessToken = environment.mapbox.accessToken;
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: 17,
      center: [this.lng, this.lat]
    });

    this.popup = new mapboxgl.Popup().setHTML(
      '<b>University of Franche-Comté</b><br>' +
      'UFR STGI<br>' +
      'DISC - NUMERICA<br>' +
      'Portes du Jura, 2 cours Louis Leprince Ringuet, 25200 MONTBELIARD'
    );

    this.marker = new mapboxgl.Marker()
      .setLngLat([this.lng,this.lat])
      .setPopup(this.popup)
      .addTo(this.map)
      .togglePopup();
  }

}
