import { Injectable } from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LinksService {

  constructor(private router: Router) { }

  toProgrammableMatterTP1() {
    this.router.navigate(['/education/programmable-matter/tp1']);
  }

  toProgrammableMatterTP2(){
    this.router.navigate(['/education/programmable-matter/tp2']);
  }

  toProgrammableMatterTP3part1() {
    this.router.navigate(['/education/programmable-matter/visible-sim-1']);
  }

  toProgrammableMatterTP3part2() {
    this.router.navigate(['/education/programmable-matter/visible-sim-2']);
  }

  toProgrammableMatterTP4(){
    this.router.navigate(['/education/programmable-matter/visible-sim-3'])
  }

  toProgrammableMatterTP5(){
    this.router.navigate(['/education/programmable-matter/visible-sim-4'])
  }

  toProgrammableMatterVSInterface(){
    this.router.navigate(['/education/programmable-matter/visible-sim-5'])
  }

  toBBinstall() {
    this.router.navigate(['/education/programmable-matter/installBB']);
  }

  toVSinstall() {
    this.router.navigate(['/education/programmable-matter/installVS']);
  }
}
