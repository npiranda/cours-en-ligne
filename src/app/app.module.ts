import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { EducationComponent } from './education/education.component';
import { ResearchComponent } from './research/research.component';
import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProgrammableMatterTp1Component } from './education/programmable-matter/programmable-matter-tp1/programmable-matter-tp1.component';
import {HIGHLIGHT_OPTIONS, HighlightModule} from 'ngx-highlightjs';
import {HighlightJsModule} from 'ngx-highlight-js';
import { ProgrammableMatterTp2Component } from './education/programmable-matter/programmable-matter-tp2/programmable-matter-tp2.component';
import { InstallingBlinkyBlocksComponent } from './education/programmable-matter/installing-blinky-blocks/installing-blinky-blocks.component';
import { VisibleSim1Component } from './education/programmable-matter/visible-sim1/visible-sim1.component';
import { VisibleSim2Component } from './education/programmable-matter/visible-sim2/visible-sim2.component';
import {HttpClientModule} from '@angular/common/http';
import { ThesisComponent } from './thesis/thesis.component';
import { FooterComponent } from './footer/footer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import { BackToTopComponent } from './back-to-top/back-to-top.component';
import { InstallingVSComponent } from './education/programmable-matter/installing-vs/installing-vs.component';
import {YouTubePlayer, YouTubePlayerModule} from '@angular/youtube-player';
import {ChartsModule} from 'ng2-charts';
import {NgxMasonryModule} from 'ngx-masonry';
import { AboutComponent } from './about/about.component';
import { MapComponent } from './map/map.component';
import { PaperComponent } from './paper/paper.component';
import { WildcardComponent } from './wildcard/wildcard.component';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {CommonModule} from '@angular/common';
import {ToastrModule} from 'ngx-toastr';
import { VisibleSim3Component } from './education/programmable-matter/visible-sim3/visible-sim3.component';
import { VisibleSim4Component } from './education/programmable-matter/visible-sim4/visible-sim4.component';
import { VisibleSim5Component } from './education/programmable-matter/visible-sim5/visible-sim5.component';
const appRoutes: Routes = [
  {path: 'education', component: EducationComponent},
  {path: 'education/programmable-matter/tp1', component: ProgrammableMatterTp1Component},
  {path: 'education/programmable-matter/tp2', component: ProgrammableMatterTp2Component},
  {path: 'education/programmable-matter/installBB', component: InstallingBlinkyBlocksComponent},
  {path: 'education/programmable-matter/installVS', component: InstallingVSComponent},
  {path: 'education/programmable-matter/visible-sim-1', component: VisibleSim1Component},
  {path: 'education/programmable-matter/visible-sim-2', component: VisibleSim2Component},
  {path: 'education/programmable-matter/visible-sim-3',component: VisibleSim3Component},
  {path: 'education/programmable-matter/visible-sim-4',component: VisibleSim4Component},
  {path: 'education/programmable-matter/visible-sim-5',component: VisibleSim5Component},
  {path: 'publications', component: ResearchComponent},
  {path: 'publications/:id', component: PaperComponent},
  {path: 'research', component: ThesisComponent},
  {path: 'about', component: AboutComponent},
  {path: '', component: HomeComponent},
  {path: '**', component: WildcardComponent}
];
const routerOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
  scrollOffset: [0, 64],
  useHash: true
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    EducationComponent,
    ResearchComponent,
    HomeComponent,
    ProgrammableMatterTp1Component,
    ProgrammableMatterTp2Component,
    InstallingBlinkyBlocksComponent,
    VisibleSim1Component,
    VisibleSim2Component,
    ThesisComponent,
    FooterComponent,
    BackToTopComponent,
    InstallingVSComponent,
    AboutComponent,
    MapComponent,
    PaperComponent,
    WildcardComponent,
    VisibleSim3Component,
    VisibleSim4Component,
    VisibleSim5Component

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes, routerOptions),
    HighlightModule,
    HighlightJsModule,
    NgbModule,
    NoopAnimationsModule,
    YouTubePlayerModule,
    ChartsModule,
    NgxMasonryModule,
    ClipboardModule,
    CommonModule,
    NgbModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [{
    provide: HIGHLIGHT_OPTIONS,
    useValue: {
      fullLibraryLoader: () => import('highlight.js')
    }
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
