import { Component } from '@angular/core';
import firebase from 'firebase';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CoursEnLigne';
  constructor() {
    const config = {
      apiKey: 'AIzaSyA-ghR9KIBwWVtvzFxShFp6sGoZa5sSj2k',
      authDomain: 'benoit-piranda.firebaseapp.com',
      projectId: 'benoit-piranda',
      storageBucket: 'benoit-piranda.appspot.com',
      messagingSenderId: '833740930755',
      appId: '1:833740930755:web:7e70b16b9471bf21df6c76',
      measurementId: 'G-K3SZPE8DRS'
    };
    firebase.initializeApp(config);

    firebase.analytics();
  }
}
