import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LinksService} from '../services/links.service';

@Component({
  selector: 'app-thesis',
  templateUrl: './thesis.component.html',
  styleUrls: ['./thesis.component.css']
})
export class ThesisComponent implements OnInit {
  publication: any = [];
  collapseNewThesis: boolean = true;

  constructor(private httpClient: HttpClient, private linksService: LinksService) { }

  ngOnInit(): void {
    this.httpClient.get('assets/codirected.json').subscribe(data => {
      this.publication = data;
    });
  }

  scrollTo($element) {
    $element.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest'
    });
  }

  toInstallTutorial() {
    this.linksService.toVSinstall();
  }

  toVSTP1() {
    this.linksService.toProgrammableMatterTP3part1()
  }

  toBBInstall() {
    this.linksService.toBBinstall();
  }

  toBBTP1() {
    this.linksService.toProgrammableMatterTP1();
  }

  toBBTP2() {
    this.linksService.toProgrammableMatterTP2();
  }
}
