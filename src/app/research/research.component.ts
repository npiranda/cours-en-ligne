import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Color, Label, SingleDataSet} from 'ng2-charts';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-research',
  templateUrl: './research.component.html',
  styleUrls: ['./research.component.css']
})
export class ResearchComponent implements OnInit {

  constructor(private httpClient: HttpClient, private toastr: ToastrService) { }
  articles: any = [];
  hellothere =  'Hello there';
  categories: any = [];

  mapACL: any = [];
  ACLbyYear: any = [];
  totalACL: number = 0;

  mapACTI: any = [];
  ACTIbyYear: any = [];
  totalACTI: any = [];

  mapKeynote: any = [];
  keynoteByYear: any = [];
  totalKeynote: any = [];

  mapACF: any = [];
  ACFbyYear: any = [];
  totalACF: any = [];

  mapOther: any = [];
  otherByYear: any = [];
  totalOther: any = [];

  years: any[] = [];
  currentYear = new Date().getFullYear();
  lineChartData: ChartDataSets[] = [];
  pieChartData: SingleDataSet = [];


  ngOnInit(): void {
    this.httpClient.get('assets/data.json').subscribe(data => {
      this.articles = data;

      this.mapACL = new Map();
      this.mapACTI = new Map();
      this.mapKeynote = new Map();
      this.mapACF = new Map();
      this.mapOther = new Map();

      for (let i = 1995; i <= this.currentYear; i++) {
        this.mapACL.set(i,0);
        this.mapACTI.set(i,0);
        this.mapKeynote.set(i,0);
        this.mapACF.set(i,0);
        this.mapOther.set(i,0);
      }


      for (let i = 0; i <= this.currentYear - 1995 ; i++){
        this.years[i] = 1995 + i;
      }

      for (const article of this.articles) {
        for (const publication of article.published) {
          for (const data of publication.paperData) {
            if (data.category === 'ACL'){
              this.mapACL.set(article.year, this.mapACL.get(article.year)+1);
              this.totalACL++;
            } else if (data.category === 'ACTI'){
              this.mapACTI.set(article.year, this.mapACTI.get(article.year)+1);
              this.totalACTI++;
            } else if (data.category === 'keynote'){
              this.mapKeynote.set(article.year, this.mapKeynote.get(article.year)+1);
              this.totalKeynote++;
            } else if (data.category === 'ACF'){
              this.mapACF.set(article.year, this.mapACF.get(article.year)+1);
              this.totalACF++;
            } else if (data.category === 'other'){
              this.mapOther.set(article.year, this.mapOther.get(article.year)+1);
              this.totalOther++;
            }
          }
        }
      }

      for (let i = 1995; i <= this.currentYear ; i++) {
        this.ACLbyYear[i - 1995] = this.mapACL.get(i);
        this.ACTIbyYear[i - 1995] = this.mapACTI.get(i);
        this.keynoteByYear[i - 1995] = this.mapKeynote.get(i);
        this.ACFbyYear[i - 1995] = this.mapACF.get(i);
        this.otherByYear[i - 1995] = this.mapOther.get(i);
      }


      this.lineChartData = [
        { data: this.ACLbyYear, label: 'ACL', stack: 'a'},
        { data: this.ACTIbyYear, label: 'ACTI', stack: 'a' },
        { data: this.keynoteByYear, label: 'Keynote', stack: 'a' },
        { data: this.ACFbyYear, label: 'ACF', stack: 'a' },
        { data: this.otherByYear, label: 'Other', stack: 'a' }
      ];

      this.pieChartData = [this.totalACL, this.totalACTI, this.totalKeynote, this.totalACF, this.totalOther];

    });

  }


  public pieChartLabels: Label[] = ['ACL', 'ACTI', 'Keynote', 'ACF', 'Other'];

  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieColors = [
    {
      backgroundColor: [
        'rgba(231, 76, 60,1.0)',
        'rgba(46, 204, 113,1.0)',
        'rgba(155, 89, 182,1.0)',
        'rgba(241, 196, 15,1.0)',
        'rgba(52, 152, 219,1.0)',
      ]
    }
  ];


  lineChartLabels: Label[] = this.years;

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      backgroundColor: 'rgba(231, 76, 60,1.0)',
    },
    {
      backgroundColor: 'rgba(46, 204, 113,1.0)',
    },
    {
      backgroundColor: 'rgba(155, 89, 182,1.0)',
    },
    {
      backgroundColor: 'rgba(241, 196, 15,1.0)',
    },
    {
      backgroundColor: 'rgba(52, 152, 219,1.0)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'bar';


  getArticlesByLab(lab: string, category: string){
    let res = 0;
    for (const article of this.articles) {
      for (const publication of article.published) {
        for (const data of publication.paperData) {
          if ((data.laboratory == lab || lab == '') && (data.category == category || category == '')) {
            res++;
          }
        }
      }
    }
    return res;
  }

  getArticlesByDomain(domain: string, category: string){
    let res = 0;
    for (const article of this.articles) {
      for (const publication of article.published) {
        for (const data of publication.paperData) {
          if ((data.domain == domain || domain == '') && (data.category == category || category == '')) {
            res++;
          }
        }
      }
    }
    return res;
  }

  getArticlesByName(name: string) {
    let res = 0;
    for (const article of this.articles) {
      for (const publishedElement of article.published) {
        if (publishedElement.destID == name) {
          res++;
        }
      }
    }
    return res;
  }

  getNumber(){
    let res = 0;
    for (const article of this.articles) {
      res += article.published.length;
    }
  }



  getArticlesByYear(year: number) {
    for (const article of this.articles) {
      if (article.year === year){
        return article.published.length;
      }
    }
  }

  scrollTo(id: string) {
    document.getElementById(id).scrollIntoView();
  }

  toast(e) {
    document.getElementById('toast').style.visibility = 'visible';
    setTimeout(function() {
      document.getElementById('toast').style.visibility = 'hidden';
    }, 5000);
  }
}
