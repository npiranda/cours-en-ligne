import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisibleSim4Component } from './visible-sim4.component';

describe('VisibleSim4Component', () => {
  let component: VisibleSim4Component;
  let fixture: ComponentFixture<VisibleSim4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisibleSim4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisibleSim4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
