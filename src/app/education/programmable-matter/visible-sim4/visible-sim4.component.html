<app-header></app-header>
<div class="container">
  <div class="jumbotron">
    <h1 class="display-4">Simulate motions of a snake made of <i>Sliding Cubes</i> with <em>VisibleSim</em> simulator</h1>
    <p class="lead">This exercise consists in realizing an application which allows to show the displacement of a set of
      modules forming a simple line. The motion is obtained by successive reconfigurations of the set of robots.
      The principle is to move the overall robot assembly by successively translating the modules from one side to the
      other.
    </p>
  </div>

  <h2>Method overview</h2>

  <p>In this <em>VisibleSim</em> application, we will use robots named "<i>Sliding Cubes</i>" which can carry out 2 types of
    displacement:</p>
  <ul>
    <li>The translation to slide on a neighbor with whom it shares a face.</li>
    <li>The rotation around a common edge with a neighbor.</li>
  </ul>
<p>The following video shows a simple reconfiguration using the two kind of motions of <i>Sliding Cubes</i>:</p>
  <youtube-player
    videoId="UNgUdO2seQI"
    suggestedQuality="highres"
    width="800px"
    height="500px"
  ></youtube-player>
  <p>To achieve this exercise, you'll use message communication as well as unit movements of the <i>Sliding Cube</i> robot.
    You can use two different functions to manage the movements of the <i>Sliding Cubes</i>:</p>
  <ol>
    <li>The first one allows you to know if it exists a valid motion that allows a block to reach a specified position
      from its current one in one move:
      <samp>module->canMoveTo(position)</samp> returns <samp>true</samp> if the module can move in
      ‘<samp>position</samp>’.
    </li>
    <li>The second one applies a move (sliding or rotation) to move the module to the given position (if the move is
      possible of course): <samp>module->moveTo(position)</samp> starts moving the module to ‘<samp>position</samp>’.
      The event "<samp>onMotionEnd</samp>" is automatically generated at the end the the motion.
  </ol>

  <textarea highlight-js [options]="{}" [lang]="'c++'">
Cell3DPosition position = module->position + Cell3DPosition(0, 1, 0);
module->canMoveTo(position);
if (module->canMoveTo(posLeft)) {
    module->moveTo(posLeft);
    return true;
}
  </textarea>

  <p>Please consider the following configuration: <samp>config1.xml</samp> made of 4 modules aligned along the y axis.</p>
  <textarea highlight-js [options]="{}" [lang]="'xml'">
<?xml version="1.0" standalone="no" ?>
<vs>
   <visuals>
      <window size="1000x900" backgroundColor="#4d4dd0" />
      <render shadows="on" grid="on"/>
   </visuals>
   <goal position="1,4,0"/>
   <world gridSize="5,10,3" >
   <camera target="50,30,17" directionSpherical="60,10,80" angle="45"/>
   <spotlight target="100,10,80" directionSpherical="60,45,80" angle="25"/>

   <blockList color="128,128,128" >
      <block position="1,1,0" orientation="2" leader="true"/>
      <block position="1,2,0" orientation="4" />
      <block position="1,3,0" orientation="16" />
      <block position="1,4,0" orientation="5" />
   </blockList>
</world>
</vs>
  </textarea>

  <h2>Create a new application</h2>
  <p>The goal of the algorithm is to make the modules move on the ground so that one of them reaches the position (on
    the ground) indicated by the target module.
    The problem is that the Sliding Cubes can only move in contact with a neighbor. It is therefore necessary to organize
    successive movements.
    First, create a new application named <samp>SBsnake</samp> using the <i>VisibleSim generator</i>, based on <i>Sliding Cubes</i>.</p>
  <p>Add a message names "<samp>ElectFirst</samp>" without embed data that will be useful to elect the new mobile robot
    at the end of a motion.</p>
    <p>Select the following methods (or functions) to be integrated to the new application:</p>
  <ul>
    <li><samp>parseUserElements</samp> method to read the final position. Be careful, as this function is only called
      once for all the robots, you have to use a global variable (<samp>goalPosition </samp>) in the .cpp file, so that
      all the modules have access to this information.
      It must not be an attribute of the SBsnake class. Here an example of code:
      <textarea highlight-js [options]="{}" [lang]="'c++'">
void SBsnake::parseUserElements(TiXmlDocument *config) {
    TiXmlNode *vs = config->FirstChild("vs");
    if (!vs) return;
    TiXmlNode *node = vs->FirstChild("goal");
    if (!node) return;
    TiXmlElement *element = node->ToElement();
    const char *attr = element->Attribute("position");
    if (attr) {
        goalPosition=Simulator::extractCell3DPositionFromString(attr);
        std::cout << "goalPosition = " << goalPosition << std::endl;
    } else {
        goalPosition.set(1,5,0);
    }
}
  </textarea>    </li>

    <li><samp>parseUserBlockElements</samp> method to define which module is "leader".</li>
    <li><samp>onMotionEnd</samp> method for managing the succession of multiple movements.</li>
    <li><samp>onGlDraw</samp> method that draws the target position in the 3D interface. The following code can be add
      to your code to draw
      a circle in the cell placed at <samp>goalPosition</samp> in order to show the goal of your snake.
      <textarea highlight-js [options]="{}" [lang]="'c++'">
  void SBsnake::onGlDraw() {
    static const float thick=0.8;
    static const float color[4]={2.2f,0.2f,0.2f,1.0f};
    const Cell3DPosition& gs = lattice->gridSize;
    const Vector3D gl = lattice->gridScale;
    glDisable(GL_TEXTURE);
    glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE,color);
    glPushMatrix();
    glNormal3f(0,0,1);
    glScalef(gl[0],gl[1],gl[2]);
    glTranslatef(goalPosition[0],goalPosition[1],goalPosition[2]-0.49);
    glBegin(GL_QUAD_STRIP);
    for (int i=0; i<=36; i++) {
        double cs=0.5*cos(i*M_PI/18);
        double ss=0.5*sin(i*M_PI/18);
        glVertex3f(thick*cs,thick*ss,0);
        glVertex3f(cs,ss,0);
    }
    glEnd();
    glPopMatrix();
 }
</textarea>
    </li>
  </ul>

  <h2>Exercice</h2>
  <p>The following questions will help you to step by step create your snake application.</p>
  <ol>
    <li>Considering that the current position of a module (in grid coordinates) is stored in the attribute <samp>module->position</samp>,
      modify the code proposed by the generator in order to make the leader module move to the position <samp>goalPosition=(1,4,0)</samp>
      (marked by a red circle in the interface on Figure 1).
      This configuration sets the module #1, placed "at the end of the line" as the leader, then it has to move up
      the whole line
      to reach the final position.</li>
    <li>Then complete your program to make a more complex move allowing the successive movement of other modules so
      that one of
      the robots reaches the position <samp>goalPosition=(1,7,0)</samp>.
    </li>
    <li>Now, propose a method to allow the robot to reach the position <samp>goalPosition=(3,5,0)</samp>.
      In this case, move first along the y axis and then along the x axis.
    </li>
    <li>Consider the following configuration file (config2.xml) that adds an obstacle on the way of the snake (cf.
      Figure 2).
      The modules that compose this obstacle are marked with the attribute “obstacle=true” and drawn in blue.
      Complete your code to treat this obstacle by allowing moving modules to pass over these modules in order to
      reach the goal position.
    </li>
    <textarea highlight-js [options]="{}" [lang]="'xml'">
<?xml version="1.0" standalone="no" ?>
<vs>
    <visuals>
        <window size="1000x900" backgroundColor="#4d4dd0" />
        <render shadows="on" grid="on"/>
    </visuals>
    <goal position="1,8,0"/>
    <world gridSize="5,10,3" >
        <camera target="50,30,17" thetaPhiDist="60,10,80" fov="45"/>
        <spotlight target="100,10,80" thetaPhiDist="60,45,80" fov="25"/>

        <blockList color="128,128,128" >
            <block position="1,1,0" orientation="1" leader="true"/>
            <block position="1,2,0" orientation="4" />
            <block position="1,3,0" orientation="16" />
            <block position="1,4,0" orientation="14" />
            <block position="0,6,0" orientation="3" color="blue" obstacle="true" />
            <block position="1,6,0" orientation="5" color="blue" obstacle="true" />
            <block position="2,6,0" orientation="1" color="blue" obstacle="true" />
        </blockList>
    </world>
</vs>
  </textarea>
    <li>Finally propose a new configuration (a config file) that combines a longer snake, obstacle and a displacement
      in y and x directions.
    </li>
  </ol>

  <h2>To return</h2>
  <p>At the end of your work, please send by <a href="mailto:benoit.piranda@univ-fcomte.fr">mail</a>, individually:
  </p>
  <ul>
    <li>The code and the xmlfile you use for the exercise (<samp>applicationsSrc/myApp*.[cpp][h]</samp> and
      <samp>applicationBin/*.xml</samp>).</li>
    <li>Please, add comments on your code to explain the meaning of each kind of message</li>
    <li>To complete...</li>
  </ul>
</div>
