import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisibleSim2Component } from './visible-sim2.component';

describe('VisibleSim2Component', () => {
  let component: VisibleSim2Component;
  let fixture: ComponentFixture<VisibleSim2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisibleSim2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisibleSim2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
