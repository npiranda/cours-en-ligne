import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgrammableMatterTp2Component } from './programmable-matter-tp2.component';

describe('ProgrammableMatterTp2Component', () => {
  let component: ProgrammableMatterTp2Component;
  let fixture: ComponentFixture<ProgrammableMatterTp2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgrammableMatterTp2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammableMatterTp2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
