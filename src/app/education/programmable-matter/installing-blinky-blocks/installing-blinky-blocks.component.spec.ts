import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstallingBlinkyBlocksComponent } from './installing-blinky-blocks.component';

describe('InstallingBlinkyBlocksComponent', () => {
  let component: InstallingBlinkyBlocksComponent;
  let fixture: ComponentFixture<InstallingBlinkyBlocksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstallingBlinkyBlocksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstallingBlinkyBlocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
