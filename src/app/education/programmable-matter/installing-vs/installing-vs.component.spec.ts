import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstallingVSComponent } from './installing-vs.component';

describe('InstallingVSComponent', () => {
  let component: InstallingVSComponent;
  let fixture: ComponentFixture<InstallingVSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstallingVSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstallingVSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
