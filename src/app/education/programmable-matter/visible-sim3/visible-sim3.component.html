<app-header></app-header>
<div class="container">
  <div class="jumbotron">
    <h1 class="display-4">Self-Reconfiguration in 3D</h1>
    <p class="lead">This tutorial shows how to develop applications for VisibleSim in C++ using mobile robots moving in a 3D lattice.<br/>
      We focus here on how 3D Catoms can be used to write a general distributed self-reconfiguration program.<br/>

      We propose here an open research problem which is not solved at the end of the tutorial, but it gives all the keys
      to help you to propose many solutions.<br/>

      The goal of this tutorial is to to go as far as possible in the questions and to propose an algorithm
      (first on paper, and if possible in VisibleSim to solve the problem).
    </p>
  </div>
  <h2>Context and proposed solution</h2>
  <p>Please download <a href="https://etudiants-stgi.pu-pm.univ-fcomte.fr/tp_bpiranda/matiereProgrammable/simulatorCore.zip">this recent version</a>
    of VisibleSim simulatorCore directory, copy it in your VisibleSim directory and re-build the project.</p>
  <p>Create a new application with <a href="https://services-stgi.pu-pm.univ-fcomte.fr/visiblesim/generator.php#">VisibleSim Generator</a> as shown in the figure below.</p>
  <img src="https://etudiants-stgi.pu-pm.univ-fcomte.fr/tp_bpiranda/matiereProgrammable/images/PM3_generator.png" alt="Generator" width="50%"/>
  <h3>The initial situation</h3>
  <p>We propose an initial configuration consisting in two islands made of 3D Catoms presented in the figure below.
    The goal of the problem consists in moving green modules of the left island to place them over the blue area. <br/>
    The modules on the ground (in grey or blue) should not move, they provide a surface on which the other robots can operate.</p>
  <img src="https://etudiants-stgi.pu-pm.univ-fcomte.fr/tp_bpiranda/matiereProgrammable/images/PM3_initial.png" alt="Initial configuration" width="50%"/>
  <p>This configuration is available <a href="https://etudiants-stgi.pu-pm.univ-fcomte.fr/tp_bpiranda/matiereProgrammable/config/PM3_config.xml">here</a>,
    we define in this file a target area that cover the 4x4 modules placed over the blue ones, and one of the right island
    module (in dark blue) is marked as leader for the first step of the algorithm.</p>
  <h3>Specificity of 3D Catoms</h3>
  The 3D Catoms are placed in a FCC lattice and can move by rotating over neighbors. They may have up to 12 neighbors attached
  on one of the connectors (in red in the figure below). Two kinds of rotation are possible, following the green or the blue
  actuators. Each rotation implies a pivot, this is the module around which the mobile module rotates. After a rotation,
  the module reach a new neighbor cell of the FCC lattice and is reconnected to the set.
  <img src="https://etudiants-stgi.pu-pm.univ-fcomte.fr/tp_bpiranda/matiereProgrammable/images/PM3_catoms_rotations.png" alt="Catoms actions" width="50%"/><br/>

  They need free space around to move, in particular, it is not possible to insert a module between two others already
  in place as shown in the figure below (right part).
  <img src="https://etudiants-stgi.pu-pm.univ-fcomte.fr/tp_bpiranda/matiereProgrammable/images/PM3_insertion_OK_NOK.png" alt="Possible motions"/>
  <h3>A possible method of resolution</h3>
  <p>The idea proposed here to solve the problem consists in defining a path from a cell that must be filled on the right
    island to the farthest robot of the left island that is able to move. After defining this path, we must define a method
    to make the module to roll over modules that are part of this path.<br>
    To create this path, we will make several communications between the modules.</p>
  <ol><li>The first one to define a gradient and a spanning tree from the leader. (Top right image in the figure below)</li>
    <li>Then, we go through this to filter the modules that can move around a pivot placed closer to the leader than them.
      These modules are in color in the top right image.</li>
    <li>Finally, we elect among these last ones which one is the most distant from the leader. The bottom left image shows
      the path in white and at the end the moving module in orange.</li>
  </ol>

  <img src="https://etudiants-stgi.pu-pm.univ-fcomte.fr/tp_bpiranda/matiereProgrammable/images/PM3_algo.png" alt="Possible motions"/>

  <h2>Compute the gradient</h2>
  <p>To simplify the resolution, one of the module which is inside the final shape is marked as leader.</p>
  <p>Start the gradient using the following rules:</p>
  <ul>
    <li>
      The "distance" variable is set to 0 inside the goal shape.
    </li>
    <li>
      The "distance" variable is the shortest distance to the goal shape if the module is outside the goal shape.
    </li>
  </ul>
  The following code allow to check if the current module is inside the target area.
  <textarea highlight-js [options]="{}" [lang]="'cpp'">
target->isInTarget(module->position)
</textarea>

  <h2>Detect modules that are able to move</h2>
  <p>As a 3D Catom can only move if geometrical environment conditions are met, VisibleSim proposes a method that returns
    the list of available moves from the current position of the module.
    This method will return the list of all valid moves using all available pivots and all actuators that can be used
    for rotation.</p>
  <textarea highlight-js [options]="{}" [lang]="'cpp'">
vector<std::pair<const Catoms3DMotionRulesLink*, Catoms3DRotation>> motions =
      Catoms3DMotionEngine::getAllRotationsForModule(module);
</textarea>

  <p>The vector <samp>motions</samp> contains two data:</p>
  <ol>
    <li>The first part (<samp>Catoms3DMotionRulesLink*</samp>) gives technical aspect of the rotation.</li>
    <li>The second part (<samp>Catoms3DRotation</samp>) is more interesting for the user, it contains the pivot
      (<samp>Catoms3DBlock *pivot</samp>), and the type of actuator used.</li>
  </ol>

  <p>To use this function, you must include the two following files:</p>
  <textarea highlight-js [options]="{}" [lang]="'cpp'">
#include "robots/catoms3D/catoms3DMotionEngine.h"
#include "robots/catoms3D/catoms3DRotationEvents.h"
  </textarea>

  <p>The user must select the appropriate move from this list before making a move. The following function
    (<samp>canMove(port)</samp>) allow to check if it exists a rotation that use port (<samp>port</samp>).
    The <samp>findNeighborPort(neighbor)</samp> function returns the number of the port that is connected to module
    <samp>neighbor</samp>.
  </p>
  <textarea highlight-js [options]="{}" [lang]="'cpp'">
int C3DReconfDemoCode::findNeighborPort(const Catoms3DBlock *neighbor) {
    int i=0;
    while (i<FCCLattice::MAX_NB_NEIGHBORS && module->getNeighborBlock(i)!=neighbor) {
        i++;
    }
    return (i<FCCLattice::MAX_NB_NEIGHBORS?i:-1);
}

bool C3DReconfDemoCode::canMove(int port) {
    vector<std::pair<const Catoms3DMotionRulesLink*, Catoms3DRotation>> motions = Catoms3DMotionEngine::getAllRotationsForModule(module);
    for (auto &motion:motions) {
        int pivotPort = findNeighborPort(motion.second.pivot);
        if (pivotPort==port) return true;
    }
    return false;
}</textarea>

  <h2>Activate the motion of a 3D Catom</h2>
  <p>The rotation of a 3D Catoms is start scheduling a <samp>Catoms3DRotationStartEvent(time,module,rotation)</samp> event,
    where <samp>rotation</samp> is one of the rotation stored in the previous list.</p>
  <p>For example, the following function starts the first motion of the list for which isOnPath array cell is equal to
    true.</p>

  <textarea highlight-js [options]="{}" [lang]="'cpp'">
    void C3DReconfDemoCode::moveToFirst() {
    vector<std::pair<const Catoms3DMotionRulesLink*, Catoms3DRotation>> motions = Catoms3DMotionEngine::getAllRotationsForModule(module);
    auto motion=motions.begin();
    bool found=false;
    while (motion!=motions.end() && !found) {
        int pivotPort = findNeighborPort((*motion).second.pivot);
        if (pivotPort!=-1 && isOnPath[pivotPort]) {
            scheduler->schedule(new Catoms3DRotationStartEvent(scheduler->now() + 1000, module, (*motion).second));
            found=true;
        }
        motion++;
    }
}
</textarea>

  <h2>Work to return</h2>
  <p>You must return two documents: a report and your application code, send all by
    <a href="mailto:benoit.piranda@univ-fcomte.fr?subject=[M2IOT_TP2]">mail</a> (one per group) for November the 11th:</p>
  <ul>
    <li>Report (pdf file):</li><ol>
    <li>The group,</li>
    <li>The detailed explanation of the algorithm, presenting the global method, the list of messages with embedded data.
      You may add some figures to illustrate your method and show effects on your code.</li>
    <li>Some extracts of your code that correspond to the proposed algorithm.</li>
  </ol>
    <li>The zipped applicationSrc directory.</li>
  </ul>

</div>

