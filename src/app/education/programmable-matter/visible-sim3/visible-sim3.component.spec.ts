import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisibleSim3Component } from './visible-sim3.component';

describe('VisibleSim3Component', () => {
  let component: VisibleSim3Component;
  let fixture: ComponentFixture<VisibleSim3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisibleSim3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisibleSim3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
