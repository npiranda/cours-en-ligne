import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisibleSim1Component } from './visible-sim1.component';

describe('VisibleSim1Component', () => {
  let component: VisibleSim1Component;
  let fixture: ComponentFixture<VisibleSim1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisibleSim1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisibleSim1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
