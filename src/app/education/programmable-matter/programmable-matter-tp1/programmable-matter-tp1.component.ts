import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-programmable-matter-tp1',
  templateUrl: './programmable-matter-tp1.component.html',
  styleUrls: ['./programmable-matter-tp1.component.css']
})
export class ProgrammableMatterTp1Component implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToInstall() {
    this.router.navigate(['education/programmable-matter/installBB']);
  }
}
