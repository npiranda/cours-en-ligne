import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgrammableMatterTp1Component } from './programmable-matter-tp1.component';

describe('ProgrammableMatterTp1Component', () => {
  let component: ProgrammableMatterTp1Component;
  let fixture: ComponentFixture<ProgrammableMatterTp1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgrammableMatterTp1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammableMatterTp1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
