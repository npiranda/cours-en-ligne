import { Component, OnInit } from '@angular/core';

import {LinksService} from '../services/links.service';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {
  imgCatapulte = 'assets/img_synthesis/img_catapulte.png';
  imgCatapulte2 = 'assets/img_synthesis/img_catapulte.png';
  isAdvancedAlgorithmsCollapsed = false;
  isDistributedAlgorithmsCollapsed = false;
  isImageSynthesisCollapsed = false;
  isWebGLCollapsed = false;
  isDemosCollapsed = false;
  isStudentProdCollapsed = false;
  constructor(private linksService: LinksService) { }

  ngOnInit(): void {
  }

  toTP1(){
    this.linksService.toProgrammableMatterTP1();
  }

  toTP2() {
    this.linksService.toProgrammableMatterTP2();
  }

  toTP3part1() {
    this.linksService.toProgrammableMatterTP3part1();
  }

  toTP3part2() {
    this.linksService.toProgrammableMatterTP3part2();
  }

  toTP4(){
    this.linksService.toProgrammableMatterTP4();
  }

  toTP5(){
    this.linksService.toProgrammableMatterTP5();
  }

  toBBinstall() {
    this.linksService.toBBinstall();
  }

  toVSinstall() {
    this.linksService.toVSinstall();
  }

  scrollTo($element) {
    $element.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest'
    });
  }
}
