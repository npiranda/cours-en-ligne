import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-back-to-top',
  templateUrl: './back-to-top.component.html',
  styleUrls: ['./back-to-top.component.css']
})
export class BackToTopComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  scrollToHead() {
    let top = document.getElementById('head');
    top.scrollIntoView({
      behavior:'smooth',
      block: 'start',
      inline: 'nearest'
    });
  }
}
